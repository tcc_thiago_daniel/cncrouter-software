/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package cncrouter;

import gnu.io.*;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TooManyListenersException;

public class Communicator implements SerialPortEventListener
{
    //passed from main GUI
    CncRouter window = null;

    //for containing the ports that will be found
    private Enumeration ports = null;
    //map the port names to CommPortIdentifiers
    private HashMap portMap = new HashMap();

    //this is the object that contains the opened port
    private CommPortIdentifier selectedPortIdentifier = null;
    private SerialPort serialPort = null;

    //input and output streams for sending and receiving data
    private InputStream input = null;
    private OutputStream output = null;

    //just a boolean flag that i use for enabling
    //and disabling buttons depending on whether the program
    //is connected to a serial port or not
    private boolean bConnected = false;

    //the timeout value for connecting with the port
    final static int TIMEOUT = 2000;

    //some ascii values for for certain things
    final static int SPACE_ASCII = 32;
    final static int DASH_ASCII = 45;
    final static int NEW_LINE_ASCII = 10;

    //a string for recording what goes on in the program
    //this string is written to the GUI
    String logText = "";
    String resposta = "";
    String ultimaResposta = "";
    boolean respondido = false;

    public Communicator(CncRouter window)
    {
        this.window = window;
    }

    //search for all the serial ports
    //pre: none
    //post: adds all the found ports to a combo box on the GUI
    public void searchForPorts()
    {
        ports = CommPortIdentifier.getPortIdentifiers();
        window.getS_porta().removeAllItems();
        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                window.getS_porta().addItem(curPort.getName());
                portMap.put(curPort.getName(), curPort);
            }
        }
    }

    //connect to the selected port in the combo box
    //pre: ports are already found by using the searchForPorts method
    //post: the connected comm port is stored in commPort, otherwise,
    //an exception is generated
    public void connect()
    {
        String selectedPort = (String)window.getS_porta().getSelectedItem();
        selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);

        CommPort commPort = null;

        try
        {
            //the method below returns an object of type CommPort
            commPort = selectedPortIdentifier.open("cncRouter", TIMEOUT);
            //the CommPort object can be casted to a SerialPort object
            serialPort = (SerialPort)commPort;
            int baudrate = Integer.parseInt(window.getS_velocidade().getSelectedItem().toString());
            //System.out.println(""+baudrate);
            serialPort.setSerialPortParams(baudrate, serialPort.DATABITS_8, serialPort.STOPBITS_1, serialPort.PARITY_NONE);
            //for controlling GUI elements
            setConnected(true);

            //logging
            logText = selectedPort + " opened successfully.";
            window.getE_console().setForeground(Color.black);
            window.getE_console().append(logText + "\n");

            //CODE ON SETTING BAUD RATE ETC OMITTED
            //XBEE PAIR ASSUMED TO HAVE SAME SETTINGS ALREADY

            //enables the controls on the GUI if a successful connection is made
            //window.keybindingController.toggleControls();
        }
        catch (PortInUseException e)
        {
            logText = selectedPort + " is in use. (" + e.toString() + ")";
            
            //window.getE_console().setForeground(Color.RED);
            window.getE_console().append(logText + "\n");
        }
        catch (Exception e)
        {
            logText = "Failed to open " + selectedPort + "(" + e.toString() + ")";
            window.getE_console().append(logText + "\n");
            window.getE_console().setForeground(Color.RED);
        }
    }

    //open the input and output streams
    //pre: an open port
    //post: initialized intput and output streams for use to communicate data
    public boolean initIOStream()
    {
        //return value for whather opening the streams is successful or not
        boolean successful = false;

        try {
            //
            input = serialPort.getInputStream();
            output = serialPort.getOutputStream();
            writeData(" ");
            
            successful = true;
            return successful;
        }
        catch (IOException e) {
            logText = "I/O Streams failed to open. (" + e.toString() + ")";
            window.getE_console().setForeground(Color.red);
            window.getE_console().append(logText + "\n");
            return successful;
        }
    }

    //starts the event listener that knows whenever data is available to be read
    //pre: an open serial port
    //post: an event listener for the serial port that knows when data is recieved
    public void initListener()
    {
        try
        {
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        }
        catch (TooManyListenersException e)
        {
            logText = "Too many listeners. (" + e.toString() + ")";
            window.getE_console().setForeground(Color.red);
            window.getE_console().append(logText + "\n");
        }
    }

    //disconnect the serial port
    //pre: an open serial port
    //post: clsoed serial port
    public void disconnect()
    {
        //close the serial port
        if(bConnected){
            try
            {
                writeData(" ");

                serialPort.removeEventListener();
                serialPort.close();
                input.close();
                output.close();
                setConnected(false);
                //window.keybindingController.toggleControls();

                logText = "Disconnected.";
                //window.getE_console().setForeground(Color.red);
                window.getE_console().append(logText + "\n");
            }
            catch (Exception e)
            {
                logText = "Failed to close " + serialPort.getName() + "(" + e.toString() + ")";
                window.getE_console().setForeground(Color.red);
                window.getE_console().append(logText + "\n");
            }
        }
        
    }

    final public boolean getConnected()
    {
        return bConnected;
    }

    public void setConnected(boolean bConnected)
    {
        this.bConnected = bConnected;
    }

    //what happens when data is received
    //pre: serial event is triggered
    //post: processing on the data it reads
    public void serialEvent(SerialPortEvent evt) {
        if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE)
        {
            try
            {   
                while(input.available()>0){
                    byte singleData = (byte)input.read();

                    if (singleData != NEW_LINE_ASCII)
                    {
    //                    logText = new String(new byte[] {singleData});
    //                    window.getE_console().append(logText);
                        resposta+=(char)singleData;
                    }
                    else
                    {
                        if(resposta.equals("ok")){
                            respondido = true;
                        }else{
                            window.getE_console().append(resposta+"\n");
                            ultimaResposta = resposta;
                        }
                        resposta="";
                    }
                    
                }
            }
            catch (Exception e)
            {
                logText = "Failed to read data. (" + e.toString() + ")";
                window.getE_console().setForeground(Color.red);
                window.getE_console().append(logText + "\n");
            }
        }
    }

    //method that can be called to send data
    //pre: open serial port
    //post: data sent to the other device
    public void writeData(String dados)
    {
        try
        {
            dados += "\n";
            byte dados_b[] = new byte[dados.length()];
            for(int i = 0; i< dados.length(); i++){
                dados_b[i] = (byte)dados.charAt(i);
            }
            output.write(dados_b);
            output.flush();
            //this is a delimiter for the data
        }
        catch (Exception e)
        {
            logText = "Failed to write data. (" + e.toString() + ")";
            window.getE_console().setForeground(Color.red);
            window.getE_console().append(logText + "\n");
        }
    }
}
