/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cncrouter;

/**
 *
 * @author daniel
 */
public class EnviaArquivo implements Runnable{
    
    CncRouter window = null;
    Communicator communicator = null;
    volatile boolean rodando=true;
    String linhas[];
    int qtL = 0;
    public synchronized void pause(int t) throws InterruptedException{
         try{
             this.wait(t);
         }catch(Exception e){
             if(Thread.interrupted()){
                 window.getE_console().append("Thread posição interrompida\n");
                 Thread.currentThread().interrupt();
             }
         }
    }
    @Override
    public void run(){
        try {
          qtL = window.getE_gcode().getLineCount();
          linhas = window.getE_gcode().getText().split("\n");
         for(int i = 0;i < qtL;i++) {              
              try{
              if(!linhas[i].isEmpty()){  
                 communicator.writeData(linhas[i]); 
                 while(!communicator.respondido){pause(50);}              
                 communicator.respondido = false;
              }

              window.getE_console().append("Enviado: "+(i+1)+" de "+qtL+" linhas\n");
              }catch(ArrayIndexOutOfBoundsException e){
                  
              }
          }
          window.botoes_conectado();
          window.getPos = new Thread( window.atualizaposicao);
          window.getPos.start();
       } catch (InterruptedException e) {
           //throw new RuntimeException("Leitor de dados interrompido!", e);
       }
    }
    public synchronized void parar() {
        rodando = false;
        this.notifyAll();
    }
    public synchronized void comecar() {
        rodando = true;
        this.notifyAll();
    }
}
    

