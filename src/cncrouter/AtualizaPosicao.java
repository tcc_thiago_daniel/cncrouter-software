/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cncrouter;

/**
 *
 * @author daniel
 */
public class AtualizaPosicao implements Runnable{
    
    CncRouter window = null;
    Communicator communicator = null;
    volatile boolean rodando=true;

    public synchronized void pause(int t) throws InterruptedException{
        try{
            this.wait(t);
        }catch(Exception e){
            if(Thread.interrupted()){
                window.getE_console().append("Thread posição interrompida\n");
                Thread.currentThread().interrupt();
            }
        }
   }
    @Override
    public void run(){
        try {
          while (rodando) {
              //window.getE_console().append("oi\n");
              pause(1000);
              if(communicator.getConnected()){                
                  communicator.writeData("M114");
                  while(!communicator.respondido){pause(50);} //esperar ok
                  
                  int iniciox = communicator.ultimaResposta.indexOf("X")+2;
                  int fimx = communicator.ultimaResposta.indexOf(" ",iniciox);
                  
                  int inicioy = communicator.ultimaResposta.indexOf("Y")+2;
                  int fimy = communicator.ultimaResposta.indexOf(" ",inicioy);
                  
                  int inicioz = communicator.ultimaResposta.indexOf("Z")+2;
                  int fimz = communicator.ultimaResposta.indexOf(" ",inicioz);
                  
                  if(iniciox>=2&&fimx>=2&&inicioy>=2&&fimy>=2&&inicioz>=2&&fimz>=2){
                    String X = communicator.ultimaResposta.substring(iniciox, fimx);

                    String Y = communicator.ultimaResposta.substring(inicioy, fimy);

                    String Z = communicator.ultimaResposta.substring(inicioz, fimz);

                    window.getL_posicao().setText("X: "+X+"   Y:  "+Y+"   Z:  "+Z+"    [mm]"); 
                  }
                  communicator.respondido = false;                         
              }
          }
       } catch (InterruptedException e) {
           //throw new RuntimeException("Leitor de dados interrompido!", e);
           window.getE_console().append("Thread posição interrompida\n");
       }
    }
    public synchronized void parar() {
        rodando = false;
        this.notifyAll();
    }
    public synchronized void comecar() {
        rodando = true;
        this.notifyAll();
    }
}
    

